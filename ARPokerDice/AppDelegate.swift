//
//  AppDelegate.swift
//  ARPokerDice
//
//  Created by Ievgen Gavrysh on 8/30/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

