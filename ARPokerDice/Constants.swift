//
//  Constants.swift
//  ARPokerDice
//
//  Created by gvr on 9/5/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import Foundation


let kEarthRadius: Float = 3_959;
let kMoonRadius: Float = 1_079;
let kEarthMoonDistance: Float = 238_900
let kEarthModelRadius: Float = 0.5
let kEarthMoonMockDistance: Float = 0.3

let kARTrackingStateMsgExcessiveMotion = "Tracking: Limited due to excessive motion!"
let kARTrackingStateMsgInsufficientFeatures = "Tracking: Limited due to insufficient features!"
let kARTrackingStateMsgInit = "Tracking: Initializing..."
let kARTrackingStateMsgRelocalizing = "Tracking: Relocating..."
let kARTrackingStateMsgNotAvailable = "Tracking: Not Available!"
let kARTrackingStateMsgAllGood = "Tracking: All Good!"
let kARSessionWasInterruptedMsg = "AR Session Was Interrupted!"
let kARSessionInterruptionEndedMsg = "AR  Session Interruption Ended"
let kARSessionFailureMsg = "AR Session Failure:"
