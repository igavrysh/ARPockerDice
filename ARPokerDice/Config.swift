//
//  Config.swift
//  ARPokerDice
//
//  Created by Ievgen Gavrysh on 9/1/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let DidConfigUpdateNotification = Notification.Name(rawValue: "DidConfigUpdateNotification")
}

class Config {
    
    class var sharedInstance: Config {
        struct singletonWrapper {
            static let singleton = Config()
        }
        
        return singletonWrapper.singleton
    }
    
    var isRealDistanceMode = true {
        didSet {
            NotificationCenter.default.post(
                name: .DidConfigUpdateNotification,
                object: nil
            )
        }
    }
    
    var earthRadiusScale: Float = 1 {
        didSet {
            NotificationCenter.default.post(
                name: .DidConfigUpdateNotification,
                object: nil
            )
        }
    }
    
    var earthMoonDistanceScale: Float = 1 {
        didSet {
            NotificationCenter.default.post(
                name: .DidConfigUpdateNotification,
                object: nil
            )
        }
    }
}
