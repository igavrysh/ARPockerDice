//
//  ConfigViewControllerTableViewController.swift
//  ARPokerDice
//
//  Created by Ievgen Gavrysh on 9/1/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import UIKit

class ConfigViewController: UITableViewController {

    @IBOutlet weak var realDistanceModeSwitch: UISwitch!
    
    var showDistanceToMoonSection = false
    
    open var parentVC: ViewController!
    
    @IBOutlet weak var earthRadiusSlider: UISlider!
    
    @IBOutlet weak var distanceToMoonSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUpdate()
        
        self.showDistanceToMoonSection = !Config.sharedInstance.isRealDistanceMode
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(ViewController.configUpdate),
            name: Notification.Name.DidConfigUpdateNotification,
            object: nil
        )
    }
    
    @IBAction func onDone(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onRealDistanceModeSwitchValueChange(_ sender: UISwitch) {
        Config.sharedInstance.isRealDistanceMode = sender.isOn
    }
    
    @objc func configUpdate() {
        let config = Config.sharedInstance
        
        self.showDistanceToMoonSection = !config.isRealDistanceMode
        
        self.realDistanceModeSwitch.isOn = config.isRealDistanceMode
        
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if !self.showDistanceToMoonSection {
            return 2
        } else {
            return super.numberOfSections(in: tableView)
        }
    }
    
    @IBAction func onEarthRadiusChange(_ sender: Any) {
    }
    
    @IBAction func onEarthRadiusDragInside(_ sender: Any) {
        print("drug inside")
        
        self.parentVC.scaleValue = self.earthRadiusSlider.value
        
        //Config.sharedInstance.earthRadiusScale = self.earthRadiusSlider.value
    }
    
    @IBAction func onEarthRadiusDragOutside(_ sender: Any) {
        print("drug outside")
    }
    
    @IBAction func onDistanceToMoonChange(_ sender: Any) {
    }
}
