//
//  ViewController.swift
//  ARPokerDice
//
//  Created by Ievgen Gavrysh on 8/30/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

extension SCNVector3 {
    func length() -> Float {
        return sqrtf(x * x + y * y + z * z)
    }
}

func - (l: SCNVector3, r: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(l.x - r.x, l.y - r.y, l.z - r.z)
}

func distanceBetweenSCN3Vectors(v1: SCNVector3 , v2: SCNVector3) -> Float {
    return (v1 - v2).length()
}

func centerPivot(for node: SCNNode) -> SCNMatrix4 {
    var min = SCNVector3Zero
    var max = SCNVector3Zero
    node.__getBoundingBoxMin(&min, max: &max)
    return SCNMatrix4MakeTranslation(
        min.x + (max.x - min.x)/2,
        min.y + (max.y - min.y)/2,
        min.z + (max.z - min.z)/2
    )
}

class ViewController: UIViewController,
    UIPopoverPresentationControllerDelegate
{
    // MARK: - Properties
    
    var moonNode: SCNNode?
    
    var earthNode: SCNNode?
    
    var mockNode: SCNNode?
    
    var trackingStatus: String = ""

    // MARK: - Outlets
    
    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var styleButton: UIButton!
    
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    // MARK: - Initialization
    
    func initSceneView() {
        sceneView.delegate = self
        
        sceneView.showsStatistics = false
        
        sceneView.debugOptions = [
            .showPhysicsShapes,
            
            .showBoundingBoxes,
            
            .showLightInfluences,
            
            .showLightExtents,
            
            .showPhysicsFields,
            
            .showWireframe,
            
            //.renderAsWireframe,
            
            .showSkeletons,
            
            .showCreases,
            
            .showConstraints,
            
            .showCameras
        ]
    }
    
    func initScene() {
        let scene = SCNScene(named: "PockerDice.scnassets/SimpleScene.scn")!
        
        scene.isPaused = false
        
        sceneView.scene = scene
        
        self.moonNode = scene.rootNode.childNode(
            withName: "moon",
            recursively: true
        )
        
        self.earthNode = scene.rootNode.childNode(
            withName: "earth",
            recursively: false
        )
        
        if let earth = self.earthNode,
            let moon = self.moonNode
        {
            let mockNode = SCNNode.init()
            mockNode.position = earth.position
            moon.position = SCNVector3Make(0.5, 0, 9)
            mockNode.addChildNode(moon)
            
            scene.rootNode.addChildNode(mockNode)
            
            mockNode.addAnimation(
                self.spinAnimation(with: 27),
                forKey: "spin around"
            )
            
            self.mockNode = mockNode
            
            earth.addAnimation(
                self.spinAnimation(with: 1),
                forKey: "spin around"
            )
            
            moon.addAnimation(
                self.spinAnimation(with: 27),
                forKey: "spin around"
            )
        }
        
        self.scaleWorld(with: 1)
    }
    
    func initARSession() {
        guard ARWorldTrackingConfiguration.isSupported else {
            print("*** ARConfig: AR World Tracking Not Supported")
            
            return
        }
        
        let configuration = ARWorldTrackingConfiguration()
        
        configuration.worldAlignment = .gravity
        
        configuration.providesAudioData = false
        
        self.sceneView.session.run(configuration)
    }
    
    // MARK: - Public
    
    func setupViewController() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func subscribeToNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(ViewController.configUpdate),
            name: Notification.Name.DidConfigUpdateNotification,
            object: nil
        )
    }
    
    // MARK: - Private

    public var scaleValue: Float = 1 {
        didSet {
            self.scaleWorld(with: self.scaleValue)
            print("didSet")
        }
    }
    
    func scaleWorld(with ratio: Float) {
        if let earthNode = self.earthNode,
            let _ = self.moonNode,
            let _ = self.mockNode
        {
            let (min, max) = earthNode.boundingBox
            let currentRadius = (max.x - min.x) / 2
            let scale
                = ratio
                    * kEarthModelRadius
                    / currentRadius
            
            earthNode.runAction(.customAction(duration: 5, action: { (node, progress) in
                earthNode.scale = SCNVector3(x: scale, y: scale, z: scale)
            }))
        }
    }
    
    func spinAnimation(with duration: TimeInterval) -> CABasicAnimation {
        let spin = CABasicAnimation.init(keyPath: "rotation")
        
        let spinVectorFrom = SCNVector4(x: 0, y: 1, z: 0, w: 0)
        spin.fromValue = NSValue(scnVector4:spinVectorFrom)
        
        let spinVectorTo =  SCNVector4(
            x:0,
            y: 1,
            z: 0,
            w: Float(-2 * Double.pi)
        )
        spin.toValue = NSValue(scnVector4:spinVectorTo)
        
        spin.duration = duration
        spin.repeatCount = .infinity
        
        return spin
    }
    
    @objc func configUpdate() {
        
        if let moonNode = self.moonNode {
            //self.scaleWorld()
            
            if !Config.sharedInstance.isRealDistanceMode {
                moonNode.position = SCNVector3Make(0.5, 0, 0.3)
            } else {
                moonNode.position = SCNVector3Make(0.5, 0, 9)
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func startButtonPressed(_ sender: Any) {
    }

    // MARK: - View Managment
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initSceneView()
        self.initScene()
        self.initARSession()
        self.setupViewController()
        self.subscribeToNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton {
            segue.destination.popoverPresentationController?.sourceRect
                = button.bounds
            if let configViewController = segue.destination as? ConfigViewController {
                configViewController.popoverPresentationController?.delegate = self
                configViewController.popoverPresentationController?
                    .canOverlapSourceViewRect = false
                
                configViewController.parentVC = self
            }

        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController)
        -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.none
    }
}

extension ViewController : ARSCNViewDelegate {
    
    // MARK: - SceneKit Management
    
    func renderer(
        _ renderer: SCNSceneRenderer,
        updateAtTime time: TimeInterval)
    {
        DispatchQueue.main.async {
            self.statusLabel.text = self.trackingStatus
        }
    }
    
    // MARK: - Session State Management
    
    func session(
        _ session: ARSession,
        cameraDidChangeTrackingState camera: ARCamera)
    {
        switch camera.trackingState {
        case .notAvailable:
            self.trackingStatus = kARTrackingStateMsgNotAvailable
        case .normal:
            self.trackingStatus = kARTrackingStateMsgAllGood
        case .limited(let reason):
            switch reason {
            case .excessiveMotion:
                self.trackingStatus = kARTrackingStateMsgExcessiveMotion
            case .insufficientFeatures:
                self.trackingStatus = kARTrackingStateMsgInsufficientFeatures
            case .initializing:
                self.trackingStatus = kARTrackingStateMsgInit
            case .relocalizing:
                self.trackingStatus = kARTrackingStateMsgRelocalizing
            }
        }
    }
    
    // MARK: - Session Error Management
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        self.trackingStatus = "\(kARSessionFailureMsg) \(error)"
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        self.trackingStatus = kARSessionWasInterruptedMsg
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        self.trackingStatus = kARSessionInterruptionEndedMsg
    }
    
    // MARK: - Plane Management
}
